import matplotlib.pyplot as plt
import numpy as np
import pandas
import seaborn as sns
from matplotlib import cm
from scipy import stats
from mpl_toolkits.mplot3d import Axes3D


def plot():
    plt.plot()
    plt.xlabel('x')
    plt.ylabel('y')
    plt.title('title')
    plt.legend()
    plt.savefig('pic.png')


def gradient_descent(alpha, m, x, y, ep=0.01, max_iter=1500):
    converged = False
    iter = 0

    t0 = 0
    t1 = 0

    J = np.sum([(t0 + t1 * x[i] - y[i]) ** 2 for i in range(m)])

    cost = []

    while not converged:
        grad0 = 1.0 / m * np.sum([(t0 + t1 * x[i] - y[i]) for i in range(m)])
        grad1 = 1.0 / m * np.sum([(t0 + t1 * x[i] - y[i]) * x[i] for i in range(m)])

        temp0 = t0 - alpha * grad0
        temp1 = t1 - alpha * grad1

        cost.append(J)

        t0 = temp0
        t1 = temp1

        e = np.sum([(t0 + t1 * x[i] - y[i]) ** 2 for i in range(m)])

        if abs(J - e) <= ep:
            print('Converged, iterations: ', iter, '!!!')
            converged = True

        J = e
        iter += 1

        if iter == max_iter:
            print('Max interactions exceeded!')
            converged = True

    return t0, t1, cost


def gradient_descent2(alpha, m, x, y, ep=0.01, max_iter=1500, landa=0.1):
    converged = False
    iter = 0

    t0 = 0
    t1 = 0
    t2 = 0

    J = np.sum([(t0 + t1 * x[i] + t2 * (x[i] ** 2) - y[i]) ** 2 for i in range(m)]) + \
        landa * ((t1 ** 2) + (t2 ** 2))

    while not converged:
        grad0 = 1.0 / m * np.sum([(t0 + t1 * x[i] + t2 * (x[i] ** 2) - y[i]) for i in range(m)])
        grad1 = 1.0 / m * np.sum([(t0 + t1 * x[i] + t2 * (x[i] ** 2) - y[i]) * x[i] + landa * t1 for i in range(m)])
        grad2 = 1.0 / m * np.sum([(t0 + t1 * x[i] + t2 * (x[i] ** 2) - y[i]) * x[i] + landa * t2 for i in range(m)])

        temp0 = t0 - alpha * grad0
        temp1 = t1 - alpha * grad1
        temp2 = t2 - alpha * grad2

        t0 = temp0
        t1 = temp1
        t2 = temp2

        e = np.sum([(t0 + t1 * x[i] + t2 * (x[i] ** 2) - y[i]) ** 2 for i in range(m)]) + \
            landa * ((t1 ** 2) + (t2 ** 2))

        if abs(J - e) <= ep:
            print('Converged, iterations: ', iter, '!!!')
            converged = True

        J = e
        iter += 1

        if iter == max_iter:
            print('Max interactions exceeded!')
            converged = True

    return t0, t1, t2


def fi(z):
    return 1 / (1 + np.exp(-z))


def sigmoid_function(alpha, m, x, x2, x3, y, ep=0.01, max_iter=1500):
    converged = False
    iter = 0

    teta = np.array([0, 0, 0, 0])
    x = np.array(1, x, x2, x3)

    J = np.sum((fi(teta * x.T) - y.T) ** 2)

    while not converged:
        sigma = np.array([0, 0, 0, 0])
        for index, xi in enumerate(x):
            sigma = (fi(teta.T * xi) - y[index]) * xi
        grad = alpha / m * sigma
        teta -= grad

        e = np.sum((fi(teta * x.T) - y.T) ** 2)

        if abs(J - e) <= ep:
            print('Converged, iterations: ', iter, '!!!')
            converged = True

        J = e
        iter += 1

        if iter == max_iter:
            print('Max interactions exceeded!')
            converged = True

    return teta


def part3(alpha, m, x, x2, x3, y, ep=0.01, max_iter=1500):
    converged = False
    iter = 0

    t0 = 0
    t1 = 0
    t2 = 0
    t3 = 0

    J = np.sum([(t0 + t1 * x[i] + t2 * x2[i] + t3 * x3[i] - y[i]) ** 2 for i in range(m)])

    while not converged:
        grad0 = 1.0 / m * np.sum([(t0 + t1 * x[i] + t2 * x2[i] + t3 * x3[i] - y[i]) for i in range(m)])
        grad1 = 1.0 / m * np.sum([(t0 + t1 * x[i] + t2 * x2[i] + t3 * x3[i] - y[i]) * x[i] for i in range(m)])
        grad2 = 1.0 / m * np.sum([(t0 + t1 * x[i] + t2 * x2[i] + t3 * x3[i] - y[i]) * x2[i] for i in range(m)])
        grad3 = 1.0 / m * np.sum([(t0 + t1 * x[i] + t2 * x2[i] + t3 * x3[i] - y[i]) * x3[i] for i in range(m)])

        t0 -= alpha * grad0
        t1 -= alpha * grad1
        t2 -= alpha * grad2
        t3 -= alpha * grad3

        e = np.sum([(t0 + t1 * x[i] + t2 * x2[i] + t3 * x3[i] - y[i]) ** 2 for i in range(m)])

        if abs(J - e) <= ep:
            print('Converged, iterations: ', iter, '!!!')
            converged = True

        J = e
        iter += 1

        if iter == max_iter:
            print('Max interactions exceeded!')
            converged = True

    return t0, t1, t2, t3


def main():
    iris = sns.load_dataset("iris")

    # 0
    # sns.pairplot(iris)
    # plt.savefig('all-figures.png')

    # Part 1:162 iterations - t0 = -0.0352710053221 , t1 = 0.343489165718
    # plt.plot(iris.petal_length, iris.petal_width, 'o')
    # t0, t1, cost = gradient_descent(0.01, 150, iris.petal_length, iris.petal_width)
    # plt.plot([0, 7], [t0, 7 * t1 + t0])
    # print(t0, t1)
    # plt.savefig('part1.png')
    # plt.plot(cost)
    # plt.savefig('cost')

    # Part 2: 1288 iterations - t0 = 4.56335854149 t1 = 0.058549322484 t2 = 0.058549322484
    # plt.plot(iris.petal_length, iris.sepal_length, 'o')
    # t0, t1, t2 = gradient_descent2(0.01, 150, iris.petal_length, iris.sepal_length)
    # plt.plot([i / 10 for i in range(0, 70)], [t0 + t1 * i/10 + t2 * ((i/10) ** 2) for i in range(0, 70)])
    # print(t0, t1, t2)
    # plt.savefig('part2.png')

    # Part 3:
    # Converged, iterations:  343 !!!
    # -0.108248389414 0.200435673152 0.186259631484 -0.216791301115
    trimmed_iris = iris[50:149].reset_index()
    training_data = pandas.concat([trimmed_iris[0:39], trimmed_iris[50:89]], axis=0).reset_index()
    test_data = pandas.concat([trimmed_iris[40:49], trimmed_iris[90:99]], axis=0).reset_index()
    fig = plt.figure()
    ax = Axes3D(fig)
    ax.scatter(trimmed_iris.petal_width, trimmed_iris.petal_length, trimmed_iris.sepal_width, color= \
        ['r' if color == 'versicolor' else 'b' if color == 'virginica' else 'g' for color in trimmed_iris.species])
    ax.set_xlabel('petal width')
    ax.set_ylabel('petal length')
    ax.set_zlabel('sepal width')
    print(training_data.petal_width, training_data.petal_length, training_data.sepal_width)
    t0, t1, t2, t3 = part3(0.01, 78, training_data.petal_width, training_data.petal_length, training_data.sepal_width,
                           [1 if s == 'virginica' else 0 for s in training_data.species])

    print(t0, t1, t2, t3)
    print(t0 + t1 * test_data.petal_width[16] + t2 * test_data.petal_length[16] + t3 * test_data.sepal_width[16])
    # versicolor
    # plt.show()
    # X = np.arange(-5, 5, 0.25)
    # Y = np.arange(-5, 5, 0.25)
    # X, Y = np.meshgrid(X, Y)
    # Z = -(t0 + t1 * X + t2 * Y) / t3
    # surf = ax.plot_surface(X, Y, Z, linewidth=0, antialiased=False)
    plt.savefig('part3.png')


if __name__ == "__main__":
    main()
